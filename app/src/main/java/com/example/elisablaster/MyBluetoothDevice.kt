package com.example.elisablaster

import android.bluetooth.BluetoothDevice

class MyBluetoothDevice (dev: BluetoothDevice, choice: Boolean){
    public val device = dev
    public val address = device.address
    public var isChoosenDevice = choice
}