package com.example.elisablaster.adapters
import kotlinx.android.synthetic.main.item_recycler_view_new_device.view.*

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.elisablaster.MyBluetoothDevice
import com.example.elisablaster.R

import java.util.ArrayList

class RecyclerViewNewDevicesAdapter(var items: ArrayList<MyBluetoothDevice>) :
    RecyclerView.Adapter<RecyclerViewNewDevicesAdapter.ViewHolderNewDevice>()  {

    override fun getItemCount() = items.size

    fun addItem(myBlthDev: MyBluetoothDevice) {
        items.add(myBlthDev)
        notifyDataSetChanged()
    }

    fun removeAllItem() {
        if (items.size != 0) {
            items.clear()
            notifyDataSetChanged()
        }

    }

    fun removeItem(device: BluetoothDevice):Unit {
        for (myDevice in items) {
            if (myDevice.device.address.contains(device.address)) {
                items.remove(myDevice)
                break
            }
        }
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderNewDevice {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_view_new_device, parent, false)
        return ViewHolderNewDevice(parent.context, view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolderNewDevice, position: Int) {

        val myBluetoothDevice = items[position]
        viewHolder.txtDevName.text = myBluetoothDevice.device.name
        viewHolder.txtDevAddr.text = myBluetoothDevice.device.address
        if (myBluetoothDevice.isChoosenDevice) {
            viewHolder.txtDevName.setTextColor(ContextCompat.getColor(viewHolder.context,
                R.color.gray
            ))
            viewHolder.txtDevAddr.setTextColor(
                ContextCompat.getColor(viewHolder.context,
                R.color.gray
            ))
            viewHolder.txtStatCon.visibility = View.VISIBLE
        } else {
            viewHolder.txtDevName.setTextColor(ContextCompat.getColor(viewHolder.context,
                R.color.black
            ))
            viewHolder.txtDevAddr.setTextColor(ContextCompat.getColor(viewHolder.context,
                R.color.black
            ))
            viewHolder.txtStatCon.visibility = View.GONE
        }

    }


    class ViewHolderNewDevice(var context: Context, itemView: View) : RecyclerView.ViewHolder(itemView)  {

        var txtDevName: TextView = itemView.txtNewDevName
        var txtDevAddr: TextView = itemView.txtNewDevAddr
        var txtStatCon: TextView = itemView.txtStatusConnection
    }
}
