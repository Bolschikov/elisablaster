package com.example.elisablaster.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.elisablaster.MyBluetoothDevice
import com.example.elisablaster.R

import kotlinx.android.synthetic.main.item_recycler_view_blth_device.view.*
import java.util.ArrayList

class RecyclerViewDevicesAdapter(var items: ArrayList<MyBluetoothDevice>) :
    RecyclerView.Adapter<RecyclerViewDevicesAdapter.ViewHolderDevice>() {

    override fun getItemCount() = items.size

    fun addItem(myBlthDev: MyBluetoothDevice) {

        if (myBlthDev.isChoosenDevice) {
            setItemsInFalse()
        }
        items.add(myBlthDev)
        notifyDataSetChanged()
    }

    fun removeAllItem() {
        items.clear()
        notifyDataSetChanged()
        //        new ArrayList<MyBluetoothDevice>().removeAll(mListDevices);
    }

    fun setItemsInFalse() {
        for (i in items.indices) {
            if (items[i].isChoosenDevice) {
                items[i].isChoosenDevice = false
                break
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderDevice {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_view_blth_device, parent, false)
        return ViewHolderDevice(parent.context, view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolderDevice, position: Int) {

        val dev = items[position]
        viewHolder.txtDevName.text = dev.device.name
        viewHolder.txtDevAddr.text = dev.device.address
        viewHolder.radioButton.isChecked = dev.isChoosenDevice
//        if (myBluetoothDevice.getmIschosenDevice())
//            viewHolder.radioButton.isChecked = true
//        else
//            viewHolder.radioButton.isChecked = false

    }


    class ViewHolderDevice(var context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txtDevName: TextView = itemView.txtDevName
        var txtDevAddr: TextView = itemView.txtDevAddr
        var radioButton: RadioButton = itemView.radioButtonDev

        //        @Override
        //        public void onClick(View view) {
        //            int position = getAdapterPosition(); // gets item position
        //            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
        //                // We can access the data within the views
        ////                Toast.makeText(context, Integer.toString(position), Toast.LENGTH_SHORT).show();
        //            }
        //        }
    }
}
