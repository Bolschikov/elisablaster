package com.example.elisablaster.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.example.elisablaster.R
import com.example.elisablaster.dialog.BlthSettingsDialog


class SettingsFragment : PreferenceFragmentCompat(){

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref, rootKey)
        findPreference<Preference>("dfBlth")!!.setOnPreferenceClickListener {
            openBlthDialog()
            return@setOnPreferenceClickListener true
        }
    }


    fun openBlthDialog(){
        val dialogBlthSettingsDialog: BlthSettingsDialog = BlthSettingsDialog.newInstance()
        dialogBlthSettingsDialog.show(getFragmentManager()!!, "BlthSettingsDialog")
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = SettingsFragment()
    }


}
