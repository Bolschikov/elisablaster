package com.example.elisablaster.Utils

import android.content.Context
import android.content.SharedPreferences
import java.util.ArrayList

class SPUtils {

    companion object Utils{
        val SAVED_MAC_ADDRESS_PREF = "saved_address"
        val SAVED_MAC_ADDRESS = "address"

        val SAVED_ADRESS_NAME_PREF = "saved_address_name"
        val SAVED_ADRESS_NAME = "address"

        val SAVED_COLORS_PREF = "saved_colors"
        val SAVED_COLORS = "colors"
        val SIZEOFLIST = "size"

        val SAVED_GAME_MEM_MAX_SCORE_PREF = "game_mem_max_score"
        val SAVED_GAME_MEM_MAX_SCORE = "max_score"


        val SAVED_ACTIVITY_MAIN_PREF = "activity_main"
        val SAVED_FRAGMENT_RGB_PREF = "fragment_rgb"
        val SAVED_FRAGMENT_MODES_PREF = "fragment_modes"
        val SAVED_ACTIVITY_SETTINGS_PREF = "activity_settings"
        val SAVED_FIRST_OPEN = "first_open"

        var sPrefSavedMacAddress: SharedPreferences? = null

        fun put(context: Context, fileName: String, key: String, `object`: Any): Boolean {
            val sp = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)
            val editor = sp.edit()
            if (`object` is String) {
                editor.putString(key, `object`)
            } else if (`object` is Int) {
                editor.putInt(key, `object`.toInt())
            } else if (`object` is Boolean) {
                editor.putBoolean(key, `object`.toString().toBoolean())
            } else if (`object` is Float) {
                editor.putFloat(key, `object`.toFloat())
            } else if (`object` is Long) {
                editor.putLong(key, `object`.toLong())
            } else if (`object` is Set<*>) {
                editor.putStringSet(key, `object` as Set<String>)
            } else {
                editor.putStringSet(key, `object` as Set<String>)
            }

            return editor.commit()
        }


        fun get(context: Context, key: String, defaultObject: Any, FILE_NAME: String): Any? {
            val sp = context.getSharedPreferences(
                FILE_NAME,
                Context.MODE_PRIVATE
            )

            if (defaultObject is String) {
                return sp.getString(key, defaultObject)
            } else if (defaultObject is Int) {
                return sp.getInt(key, defaultObject)
            } else if (defaultObject is Boolean) {
                return sp.getBoolean(key, defaultObject)
            } else if (defaultObject is Float) {
                return sp.getFloat(key, defaultObject)
            } else if (defaultObject is Long) {
                return sp.getLong(key, defaultObject)
            }

            return null
        }

        fun putListString(context: Context, fileName: String, strList: List<String>): Boolean {
            val sp = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)
            val editor = sp.edit()
            sp.edit().clear().apply()
            editor.putInt("size", strList.size)
            for (i in strList.indices) editor.putString(Integer.toString(i), strList[i])
            return editor.commit()
        }


        fun getListString(context: Context, fileName: String): ArrayList<String> {
            val sp = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)
            val strList = ArrayList<String>()
            val sz = sp.getInt(SIZEOFLIST, 0)
            for (i in 0 until sz) strList.add(sp.getString(Integer.toString(i), null))
            return strList
        }


        fun contains(context: Context, key: String, FILE_NAME: String): Boolean {
            val sp = context.getSharedPreferences(
                FILE_NAME,
                Context.MODE_PRIVATE
            )
            return sp.contains(key)
        }


        fun clear(context: Context, FILE_NAME: String) {
            val sp = context.getSharedPreferences(
                FILE_NAME,
                Context.MODE_PRIVATE
            )
            sp.edit().clear().apply()
        }
    }



}