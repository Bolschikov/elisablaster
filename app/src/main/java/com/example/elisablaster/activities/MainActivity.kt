package com.example.elisablaster.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.core.graphics.drawable.toBitmap
import com.example.elisablaster.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseBlthActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cardSearch.setOnClickListener(this)
        cardCalendar.setOnClickListener(this)
        cardSettings.setOnClickListener(this)
        tbMain.title = ""
        setSupportActionBar(tbMain)
    }

        override fun onCreateOptionsMenu(menu: Menu?): Boolean {
            menuInflater.inflate(R.menu.menu_main, menu)
            initMenuItem(menu!!)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            cardSearch.id ->true
            cardCalendar.id -> true
            cardSettings.id -> startActivity(Intent(this, SettingsActivity::class.java))
        }
    }

}
