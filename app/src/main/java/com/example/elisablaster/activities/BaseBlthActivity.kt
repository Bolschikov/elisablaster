package com.example.elisablaster.activities

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.elisablaster.R
import com.example.elisablaster.Utils.SPUtils
import com.example.elisablaster.bluetooth.SerialListener
import com.example.elisablaster.bluetooth.SerialService
import com.example.elisablaster.bluetooth.SerialSocket
import com.example.elisablaster.dialog.ConnectingDialog
import com.example.elisablaster.dialog.NoConnectionDialog
import com.google.android.material.snackbar.Snackbar
import java.io.IOException

open class BaseBlthActivity : BaseActivity(), SerialListener,
    ServiceConnection, NoConnectionDialog.NoConnectionDialogListener{

    protected var btAdapter: BluetoothAdapter? = null
    internal var pendingRequestEnableBt = false

    private val SAVED_PENDING_REQUEST_ENABLE_BT = "PENDING_REQUEST_ENABLE_BT"
    //    public static DeviceConnector connector;
    // Message types sent from the DeviceConnector Handler

    companion object BlthInfo{
        val REQUEST_CONNECT_DEVICE = 1
        val REQUEST_ENABLE_BT = 2

        val MSG_NO_CONNECTION = 0
        val MSG_CONNECTING = 1
        val MSG_CONNECTED = 2

        var initialStart = true
        var connected = Connected.False
        var socket: SerialSocket? = null
        var service: SerialService? = null
    }


    protected var savedDevice: BluetoothDevice? = null
    protected var menuItemBlth: MenuItem? = null


    protected var mMainBtDevice: BluetoothDevice? = null

    private val numberLeds: Char = 8.toChar()  // need to put into separate class *Utils
    private val numberMaxLeds: Char = 31.toChar()
    private var millis: Long = 0
    private val dealyMillisForNextSend = 200
    private val scaleDivide = Math.pow(10.0, 5.0)


    enum class Connected {
        False, Pending, True
    }

    private var connectionDialog: ConnectingDialog? = null
    private var noConnectionDialog: NoConnectionDialog? = null

//    protected var resistValue = 0
//    protected var om = 4
//    private val R1 = 100000
//
//
//    private val uIdeal = 3300
//
//    private var temperature = 21
//
//
//    private var pressure = 700
    var flagGoToSettings = false  // need to block dialog with no connection
    var TAG = "CALL_BASE_ACTIVITY"
    private var mIsResumed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            pendingRequestEnableBt = savedInstanceState.getBoolean(SAVED_PENDING_REQUEST_ENABLE_BT)
        }
        initBTAdapter()
        millis = System.nanoTime()


    }
    // ==========================================================================

    fun initBTAdapter() {
        btAdapter = BluetoothAdapter.getDefaultAdapter()
        if (btAdapter == null) { // check blth module in phone
            val no_bluetooth = getString(R.string.no_blth)
            showAlertDialogNoBlth(no_bluetooth)
        }
    }
    // ==========================================================================

    fun checkSavedMacAddressRemoteDevice() {
        if (SPUtils.contains(applicationContext, SPUtils.SAVED_MAC_ADDRESS, SPUtils.SAVED_MAC_ADDRESS_PREF))
            savedDevice = btAdapter!!.getRemoteDevice(
                SPUtils.get(
                    applicationContext, SPUtils.SAVED_MAC_ADDRESS,
                    "null", SPUtils.SAVED_MAC_ADDRESS_PREF
                ) as String
            )
        else {
            savedDevice = null
            showNoticeDialog()
        }
    }
    // ==========================================================================

//    override fun onClick(view: View) {
//        disconnect()
//    }


    override fun onDialogNoConnectionPositiveClick(dialog: NoConnectionDialog, idError: Int) {
        when (idError) {
            0 -> {
                val i = Intent(applicationContext, SettingsActivity::class.java)
                startActivity(i)
            }
            1 -> {
                dialog.dismiss()
                connect()
            }
        }
    }
    // ==========================================================================

    override fun onDialogNoConnectionNegativeClick(dialog: NoConnectionDialog, idError: Int) {
        when (idError) {
            0 -> exitApp()
            1 -> dialog.dismiss()
        }

    }
    // ==========================================================================

    fun showNoticeDialog() {
        // Create an instance of the dialog fragment and show it

        val noConnectionDialog = NoConnectionDialog.newInstance(0)
        noConnectionDialog.show(getSupportFragmentManager(), "NoticeDialogFragment")
    }
    // ==========================================================================


    fun initMenuItem(menu: Menu) {
        menuItemBlth = menu.findItem(R.id.item_connection)
        if (connected != Connected.True)
            menuItemBlth!!.setIcon(R.drawable.ic_blth_off_black)
        else
            menuItemBlth!!.setIcon(R.drawable.ic_blth_connected)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        when (id) {
            R.id.item_connection -> {
                if (isAdapterReady() && connected == Connected.False)
                    connect()
                else
                    disconnect()
                return true
            }
        }
        return false
    }

    override fun onStart() {
        Log.e(TAG, "onStart")
        super.onStart()
        if (btAdapter == null) return
        if (!btAdapter!!.isEnabled && !pendingRequestEnableBt) {
            pendingRequestEnableBt = true
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
        }
        this.bindService(Intent(applicationContext, SerialService::class.java), this, Context.BIND_AUTO_CREATE)
        if (service != null) {
            Log.e(TAG, "attach")
            service!!.attach(this)
        } else
            this.startService(
                Intent(
                    applicationContext,
                    SerialService::class.java
                )
            ) // prevents service destroy on unbind from recreated activity caused by orientation change
    }
    // ==========================================================================


    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
        checkSavedMacAddressRemoteDevice()
        mIsResumed = true
        if (initialStart && service != null && savedDevice != null) {
            initialStart = false
            Log.e(TAG, "onResume.connect")
            this.runOnUiThread{connect()}
        }
    }


    override fun onServiceConnected(name: ComponentName, binder: IBinder) {
        Log.e(TAG, "onServiceConnected")
        service = (binder as SerialService.SerialBinder).getService()
        //        initialStart = false;
        if (initialStart && mIsResumed && savedDevice != null) {
            initialStart = false
            Log.e(TAG, "onServiceConnected.connect")
            this.runOnUiThread{ connect() }
        }
    }


    override fun onServiceDisconnected(name: ComponentName) {
        Log.e(TAG, "onServiceDisconnected")
        service = null
    }


    fun connect() {
        Log.e(TAG, "connect()")
        try {
            status(MSG_CONNECTING)
            connected = Connected.Pending
            socket = SerialSocket(applicationContext)
            service!!.connect(this, "Connected to " + savedDevice!!)
            socket!!.connect(applicationContext, service, savedDevice)
            val handler = Handler()
            handler.postDelayed({
                if (connected != Connected.True) {
                    onSerialIoError(IOException("gatt status " + "bad connection"))
                    onSerialConnectError(IOException("gatt status " + "bad connection"))
                }
            }, 8000)

        } catch (e: Exception) {
            onSerialConnectError(e)
        }

    }

//    public void connectWithoutIcon(){
//        try {
////            status(MSG_CONNECTING);
//            connected = Connected.Pending;
//            socket = new SerialSocket();
//            service.connect(this, "Connected to " + savedDevice);
//            socket.connect(getApplicationContext(), service, savedDevice);
//        } catch (Exception e) {
//            onSerialConnectError(e);
//        }
//    }

    override fun onDestroy() {
        Log.e(TAG, "onDestroy")
        //        if (connected != Connected.False)
        //            disconnect();
        //        this.stopService(new Intent(this, SerialService.class));
        super.onDestroy()

    }

    fun disconnect() {
        Log.e(TAG, "disconnect()")
        connected = Connected.False
        status(MSG_NO_CONNECTION)
        if (service != null) {
            service!!.detach()
            service!!.disconnect()
        }
        if (socket != null) socket!!.disconnect(applicationContext)
        socket = null
        initialStart = true
        //        try { this.unbindService(this); } catch(Exception ignored) {}
        //        service = null;
        //        this.stopService(new Intent(this, SerialService.class));

    }

    fun disconnectFull() {
        Log.e(TAG, "disconnect()")
        connected = Connected.False
        status(MSG_NO_CONNECTION)
        if (service != null) {
            service!!.detach()
            service!!.disconnect()
        }
        if (socket != null) socket!!.disconnect(getApplicationContext())
        socket = null
        service = null
        this.stopService(Intent(this, SerialService::class.java))

    }

    override fun onSaveInstanceState(outState: Bundle) {

        outState.putBoolean(SAVED_PENDING_REQUEST_ENABLE_BT, pendingRequestEnableBt)
        super.onSaveInstanceState(outState)
    }
    // ==========================================================================

    fun isAdapterReady(): Boolean {
        return btAdapter != null && btAdapter!!.isEnabled
    }
    // ==========================================================================

    fun showAlertDialogNoBlth(message: String) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(getString(R.string.title_attention))
        alertDialogBuilder.setMessage(message)

        alertDialogBuilder.setPositiveButton(R.string.ok) { dialog: DialogInterface, which: Int ->
            val homeIntent = Intent(Intent.ACTION_MAIN)
            homeIntent.addCategory(Intent.CATEGORY_HOME)
            homeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(homeIntent)
        }
        alertDialogBuilder.setCancelable(false)
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
    // ==========================================================================


    override fun onStop() {
        if (service != null && !this.isChangingConfigurations()) {
            Log.e(TAG, "onStop.detach")
            service!!.detach()
        }
        super.onStop()
        mIsResumed = false
        Log.e(TAG, "onStop")
    }
    // ==========================================================================


    fun sendColor(numLightsAndSensors: Byte, red: Byte, green: Byte, blue: Byte) {
        var red = red
        var green = green
        var blue = blue
        //        byte[] bytes = colorCode.getBytes();
        val info = numberLeds.toByte()
        red = checkNumInByte(red)
        green = checkNumInByte(green)
        blue = checkNumInByte(blue)
        //        mode = (byte)(mode | (byte) 128);
        //        numLightsAndSensors = (byte)(numLightsAndSensors);
        val bytesToSend = byteArrayOf(numLightsAndSensors, red, green, blue, 0x01.toByte())

        if ((System.nanoTime() - millis) / scaleDivide > dealyMillisForNextSend) {
            send(bytesToSend)
            millis = System.nanoTime()
            Log.e(
                "Bytes", Integer.toString(bytesToSend[0].toInt()) + " " +
                        Integer.toString(bytesToSend[1].toInt()) + " " +
                        Integer.toString(bytesToSend[2].toInt()) + " " +
                        Integer.toString(bytesToSend[3].toInt()) + " " +
                        Integer.toString(bytesToSend[4].toInt())
            )
        }
    }

    fun sendModes(info: Byte, hours: Byte, minutes: Byte) {
        val bytesToSend = byteArrayOf(info, hours, minutes, 0x01.toByte())
        if ((System.nanoTime() - millis) / scaleDivide > dealyMillisForNextSend) {
            send(bytesToSend)
            millis = System.nanoTime()
        }
    }


    fun sendSensor(info: Byte, s1: Byte, s2: Byte, s3: Byte, s4: Byte) {
        val bytesToSend = byteArrayOf(info, s1, s2, s3, s4, 0x01.toByte())
        if ((System.nanoTime() - millis) / scaleDivide > dealyMillisForNextSend) {
            send(bytesToSend)
            millis = System.nanoTime()
        }
    }

    fun sendMsg(msg: Byte){
        val bytesToSend = byteArrayOf(msg, msg, 0x01.toByte())
        if ((System.nanoTime() - millis) / scaleDivide > dealyMillisForNextSend) {
            send(bytesToSend)
            millis = System.nanoTime()
        }
    }


    private fun send(bytes: ByteArray) {
        Log.e(TAG, "send()")
        if (connected != Connected.True) {
            //            NoConnectionDialog noConnectionDialog = NoConnectionDialog.newInstance();
            //            noConnectionDialog.show(getSupportFragmentManager(), "add_no_connection_dialog_bottom");
            Toast.makeText(applicationContext, "not connected", Toast.LENGTH_SHORT).show()
            return
        }
        try {
            socket!!.write(bytes)
        } catch (e: Exception) {
            onSerialIoError(e)
        }

    }

//    private fun receive(data: ByteArray) {
//        val sz = data.size
//        when (sz) {
//            5 -> if (data[0] == 0x01.toByte()) {
//                var uADC = 0
//                uADC = parseReceiveData(data, 1, sz)
//                val resist = (uADC * R1).toDouble() / (uIdeal - uADC)
//                if (resist < 1000) {
//                    resistValue = 999
//                    om = 0
//                } else if (resist > 1000000) {
//                    resistValue = 999
//                    om = 2
//                } else {
//                    resistValue = resist.toInt() / 1000
//                    om = 1
//                }
//
//            }
//            6 -> if (data[0] == 0x02.toByte()) {
//                temperature = parseReceiveData(data, 1, 3)
//                pressure = parseReceiveData(data, 3, sz)
//            }
//        }
//        //        receiveText.append(new String(data));
//    }

    fun checkNumInByte(bits: Byte): Byte {
        val endOfPack: Byte = 1
        return if (bits == endOfPack)
            2.toByte()
        else
            bits
    }
    // ==========================================================================

//    private boolean isConnected() {
//        return (connector != null) && (connector.getState() == DeviceConnector.STATE_CONNECTED);
//    }
    // ==========================================================================


    private fun status(MSG: Int) {
        Log.e(TAG, "status")
        val handler = Handler()
        when (MSG) {
            MSG_NO_CONNECTION -> {
                handler.postDelayed({
                    if (menuItemBlth != null)
                        menuItemBlth!!.setIcon(R.drawable.ic_blth_off_black)
                }, 500)

                //                activity.stopConnection();
                //                connection = false;
                this.runOnUiThread(Runnable {
                    if (noConnectionDialog == null && !flagGoToSettings) {
                        try {
                            noConnectionDialog = NoConnectionDialog.newInstance(1)
                            noConnectionDialog!!.show(supportFragmentManager, "no connection")
                        } catch (ignored: IllegalStateException) {
                            // There's no way to avoid getting this if saveInstanceState has already been called.
                        }

                    }
                    val toast = Toast.makeText(
                        applicationContext,
                        getString(R.string.dialog_title_no_connection), Snackbar.LENGTH_LONG
                    )
                    toast.show()
                })
                if (connectionDialog != null) {
                    connectionDialog!!.dismiss()
                }
            }
            MSG_CONNECTING -> {
                connectionDialog = ConnectingDialog.newInstance()
                val ft = getSupportFragmentManager()
                connectionDialog!!.show(ft, "dialog")
            }
            MSG_CONNECTED -> {

                noConnectionDialog = null
                //                this.runOnUiThread(new Runnable() {
                //                    @Override
                //                    public void run() {
                //                        Toast.makeText(getApplicationContext(), "CONNECT", Toast.LENGTH_SHORT).show();
                //                    }
                //                });
                if (connectionDialog != null) {
                    connectionDialog!!.dismiss()
                }

                handler.postDelayed({ menuItemBlth!!.setIcon(R.drawable.ic_blth_connected) }, 500)
            }
        }
        //        SpannableStringBuilder spn = new SpannableStringBuilder(str+'\n');
        //        spn.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorStatusText)), 0, spn.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //        receiveText.append(spn);
    }


    override fun onSerialConnect() {
        Log.e(TAG, "onSerialConnect")
        status(MSG_CONNECTED)
        connected = Connected.True
    }

    override fun onSerialConnectError(e: Exception) {
        Log.e(TAG, "onSerialConnectError")
        status(MSG_NO_CONNECTION)
        disconnect()
    }

    override fun onSerialRead(data: ByteArray) {
        Log.e(TAG, "onSerialRead")
//        receive(data)
    }

    override fun onSerialIoError(e: Exception) {
        Log.e(TAG, "onSerialIoError")
        status(MSG_NO_CONNECTION)
        disconnect()
    }

//    @Override
//    protected boolean isResumed() {
//        return mIsResumed;
//    }


//    fun getOm(): Int {
//        return om
//    }
//
//    fun getResistValue(): Int {
//        return resistValue
//    }
//
//    fun getTemperature(): Int {
//        return temperature
//    }
//
//    fun getPressure(): Int {
//        return pressure
//    }


    override fun onPause() {
        super.onPause()
        mIsResumed = false
    }

    private fun parseReceiveData(data: ByteArray, start: Int, end: Int): Int {
        var returnData = 0
        for (i in start until end) {
            returnData += (data[i] * Math.pow(10.0, (i - start).toDouble())).toInt()
        }
        return returnData
    }

//    override fun onBackPressed() {
//        //        disconnect();
//        super.onBackPressed()
//    }

    fun exitApp() {
        val intent = Intent(this, MainActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("EXIT", true)
        startActivity(intent)
    }
}