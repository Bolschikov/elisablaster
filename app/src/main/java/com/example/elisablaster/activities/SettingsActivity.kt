package com.example.elisablaster.activities

import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.elisablaster.Fragment.SettingsFragment
import com.example.elisablaster.R

class SettingsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportFragmentManager.beginTransaction().replace(R.id.framelayoutSettings, SettingsFragment.newInstance()).commit()
    }
}
