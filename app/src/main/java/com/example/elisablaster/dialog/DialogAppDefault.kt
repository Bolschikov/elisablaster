package com.example.elisablaster.dialog

import android.graphics.Rect
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import android.R
import android.content.Context.LAYOUT_INFLATER_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.view.LayoutInflater
import android.R.layout
import android.content.Context


open class DialogAppDefault : DialogFragment() {
    override fun onResume() {
        super.onResume()

        val width = resources.getDimensionPixelSize(R.dimen.thumbnail_width)
//        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog!!.window!!.attributes)
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT)
    }
}