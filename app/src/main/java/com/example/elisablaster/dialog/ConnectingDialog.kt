package com.example.elisablaster.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import com.example.elisablaster.R

class ConnectingDialog : DialogAppDefault(){

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = ConnectingDialog()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(
            R.layout.dialog_blth_connecting, container,
            false
        )
        dialog!!.setCanceledOnTouchOutside(false)
        return view
    }
}