package com.example.elisablaster.dialog

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.elisablaster.MyBluetoothDevice
import com.example.elisablaster.R
import com.example.elisablaster.RecyclerItemClickListener
import com.example.elisablaster.Utils.SPUtils
import com.example.elisablaster.adapters.RecyclerViewDevicesAdapter
import com.example.elisablaster.adapters.RecyclerViewNewDevicesAdapter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.dialog_blth_settings.*
import java.util.HashSet

class BlthSettingsDialog : FullScreenDialog() {
    private var rvSAdapter: RecyclerViewDevicesAdapter? = null
    private var rvNAdapter: RecyclerViewNewDevicesAdapter? = null

    private var mBtAdapter: BluetoothAdapter? = null  // модуль blth
    private var mPrevChoicePairedDevices = -1
    private var mPrevChoiceNewDevices = -1  // номер выбранного девайса на предыдущем разе для 2-х RecyclerView
    private var isSearching = true

    private var mMyBluetoothDevicesAddressNewBuf: HashSet<String>? =
        null  // необходим как буффер что бы не было повторяющихся девайсов
    //    private ArrayList<MyBluetoothDevice> mMyBluetoothDevicesNew;  // хранит все найденные девайсы
    private var mSavedAddress: java.util.ArrayList<String>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_blth_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tbBlthDevice.setTitle(R.string.searching)
        tbBlthDevice.setNavigationIcon(R.drawable.ic_close)
        tbBlthDevice.setNavigationOnClickListener({ v -> dialog?.dismiss() })

        val window = activity!!.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        //        window.setStatusBarColor(getResources().getColor(R.color.green_500));
        initView()  // инициализация переменных и связанных с ними
        doDiscovery()

        rvSavedDev.addOnItemTouchListener(  // слушатель нажатия на item в RecyclerView
            RecyclerItemClickListener(
                activity!!.applicationContext, rvSavedDev,
                object : RecyclerItemClickListener.OnItemClickListener {

                    // перегрузка методов интерфейса из RecyclerItemClickListener
                    override fun onItemClick(view: View, position: Int) {

                        endDiscovery()  // Останавливаем поиск новых устройств, т.к.
                        if (mPrevChoicePairedDevices > -1)
                            rvSAdapter!!.items[mPrevChoicePairedDevices].isChoosenDevice = false
                        else
                            rvSAdapter!!.setItemsInFalse()
                        rvSAdapter!!.items[position].isChoosenDevice = true
                        mPrevChoicePairedDevices = position
                        rvSAdapter!!.notifyDataSetChanged()  // обновить вид RecyclerView для переключения RadioButton
                        Toast.makeText(activity!!.applicationContext, Integer.toString(position), Toast.LENGTH_SHORT)
                            .show()  // вывод позиции
                        saveLastMacAddress(
                            rvSAdapter!!.items[mPrevChoicePairedDevices].device.address
                        )
                    }

                    override fun onLongItemClick(view: View, position: Int) {
                        // do whatever
                    }
                })
        )

        rvNewDev!!.addOnItemTouchListener(  // слушатель нажатия на item в RecyclerView
            RecyclerItemClickListener(
                activity!!.applicationContext,
                rvNewDev,
                object : RecyclerItemClickListener.OnItemClickListener {

                    // перегрузка методов интерфейса из RecyclerItemClickListener
                    override fun onItemClick(view: View, position: Int) {

                        endDiscovery()  // Останавливаем поиск новых устройств, т.к.


                        try {
                            if (mPrevChoiceNewDevices > -1)
                                rvNAdapter!!.items[mPrevChoiceNewDevices].isChoosenDevice = false
                            rvNAdapter!!.items[position].isChoosenDevice = true
                            mPrevChoiceNewDevices = position
                        } catch (e: IndexOutOfBoundsException) {
                            mPrevChoiceNewDevices = rvNAdapter!!.items.size - 1
                        }

                        rvNAdapter!!.notifyDataSetChanged()  // обновить вид RecyclerView для переключения RadioButton
                        Toast.makeText(activity!!.applicationContext, Integer.toString(position), Toast.LENGTH_SHORT)
                            .show()  // вывод позиции
                        //                        BluetoothDevice deviceToPair = rvNewDevAdapter.getmListNewDevices().get(mPrevChoiceNewDevices).getmDevice();
                        //                        deviceToPair.createBond();
                        val device = mBtAdapter!!.getRemoteDevice(
                            rvNAdapter!!.items[mPrevChoiceNewDevices].device.address
                        )
                        Toast.makeText(activity!!.applicationContext, R.string.saved, Toast.LENGTH_SHORT)
                            .show()
                        rvNAdapter!!.removeItem(device)
                        mSavedAddress!!.add(device.getAddress())
                        rvSAdapter!!.addItem(MyBluetoothDevice(device, true))
                        //                        rvSavedDevAdapter.notifyDataSetChanged();
                        mPrevChoicePairedDevices = mSavedAddress!!.size - 1
                        if (mSavedAddress!!.size > 0)
                            noPairedDev.visibility = View.INVISIBLE
                        saveDevice(device.getAddress())
                    }

                    override fun onLongItemClick(view: View, position: Int) {}
                })
        )
    }


    fun initView() {
        initFabView()
        rvSavedDev.layoutManager = LinearLayoutManager(activity!!.applicationContext, LinearLayoutManager.VERTICAL, false)
        rvNewDev.layoutManager = LinearLayoutManager(activity!!.applicationContext, LinearLayoutManager.VERTICAL, false)

        mMyBluetoothDevicesAddressNewBuf = HashSet()
        mSavedAddress = ArrayList()
        initIntentFilter()
        mBtAdapter = BluetoothAdapter.getDefaultAdapter()  // инициализация модуля блютуз
        fillRecyclerView()

    }
    // ==========================================================================


    fun fillRecyclerView() {
        val mMyBluetoothDevicesSaved =
            java.util.ArrayList<MyBluetoothDevice>()  // инициализация списка спаренных устройств
        val savedMacAddress: String
        //        SharedPreferences.Editor.clear().apply();
        if (SPUtils.contains(
                activity!!.applicationContext,
                SPUtils.SAVED_MAC_ADDRESS,
                SPUtils.SAVED_MAC_ADDRESS_PREF
            )
        ) {
            savedMacAddress =
                SPUtils.get(
                    activity!!.applicationContext,
                    SPUtils.SAVED_MAC_ADDRESS,
                    "null",
                    SPUtils.SAVED_MAC_ADDRESS_PREF
                ) as String
            val strDeviceNameAddress = ArrayList(
                SPUtils.getListString(
                    activity!!.applicationContext,
                    SPUtils.SAVED_ADRESS_NAME_PREF
                )
            )
            for (devNameAddress in strDeviceNameAddress) {
                //                String nameDevice = devNameAddress.substring(0, 16);
                val device = mBtAdapter!!.getRemoteDevice(devNameAddress)
                //                device.getName();
                if (devNameAddress.contains(savedMacAddress))
                    mMyBluetoothDevicesSaved.add(MyBluetoothDevice(device, true))
                else
                    mMyBluetoothDevicesSaved.add(MyBluetoothDevice(device, false)) // добавление в список
                mSavedAddress!!.add(device.getAddress())
            }
            rvSAdapter = RecyclerViewDevicesAdapter(ArrayList(mMyBluetoothDevicesSaved))
            rvSavedDev.adapter = rvSAdapter

        } else {
            noPairedDev.visibility = View.VISIBLE   // условие отсутсвия спаренных устройст
            rvSAdapter = RecyclerViewDevicesAdapter(ArrayList(mMyBluetoothDevicesSaved))
            rvSavedDev.adapter = rvSAdapter


        }//        if (pairedDevices != null && !pairedDevices.isEmpty()) {  // условие на проверки полноты списка
        //            for (BluetoothDevice device : pairedDevices) {  // поочередно пробегаем каждое устройство для добавления в список
        //                if( device.getAddress().contains( savedMacAddress)){
        //                    mMyBluetoothDevicesPaired.add(new MyBluetoothDevice(device, true));
        //                } else mMyBluetoothDevicesPaired.add(new MyBluetoothDevice(device, false)); // добавление в список
        //                mPairedAddress.add(device.getAddress());
        //            }
        //            mRecyclerViewPairedDevicesAdapter = new RecyclerViewDevicesAdapter(new ArrayList<MyBluetoothDevice>(mMyBluetoothDevicesPaired));  // инициализация адаптера спиком элементов спаренных устройств
        //            mRecyclerViewPairedDevices.setAdapter(mRecyclerViewPairedDevicesAdapter);  // связывание адаптера и RecyclerView
    }
    // ==========================================================================


    fun initIntentFilter() {  // register events for scanning or boundng remote device
        val filter = IntentFilter()
        filter.addAction(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        //        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        activity!!.registerReceiver(mReceiver, filter)  // добавление события
    }
    // ==========================================================================


    fun initFabView() {
        fabSearchDev.setImageResource(R.drawable.ic_stop)  // установка icon для понимания того что поиск можно остановить
        fabSearchDev.setOnClickListener {
            if (isSearching) endDiscovery() else doDiscovery()  // сканировать
        }
    }
    // ==========================================================================


    private fun doDiscovery() {
        if (rvNAdapter != null) {
            rvNAdapter!!.removeAllItem()
        }
        mMyBluetoothDevicesAddressNewBuf!!.clear()
        fabSearchDev!!.setImageResource(R.drawable.ic_stop)  // смена icon
        tbBlthDevice?.setTitle(R.string.searching)
        progBarSearchDev!!.visibility = View.VISIBLE  // включение индикатора сканирования
        if (mBtAdapter!!.isDiscovering) mBtAdapter!!.cancelDiscovery()  // проверка на возможность сканирования в данный момент и завершение данного процесса
        isSearching = true  // флаг сканирования
        mBtAdapter!!.startDiscovery()  // сканирование
    }
    // ==========================================================================


    private fun endDiscovery() {
        progBarSearchDev!!.visibility = View.GONE  // скрыние индикатора сканирования
        fabSearchDev!!.setImageResource(R.drawable.ic_search)  // установка icon
        if (mBtAdapter!!.isDiscovering) mBtAdapter!!.cancelDiscovery()  // проверка на возможность сканирования в данный момент
        isSearching = false  // флаг сканирования
        tbBlthDevice?.setTitle("")
    }
    // ==========================================================================


    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val device: BluetoothDevice? = if(intent.action == BluetoothDevice.ACTION_FOUND) intent.getParcelableExtra(
                BluetoothDevice.EXTRA_DEVICE) as
                    BluetoothDevice else null
            when (intent.action) {
                BluetoothDevice.ACTION_FOUND -> if (device != null
                    && !mSavedAddress!!.contains(device.address)
                    && device.name != null
                    && device.name.contains(getString(R.string.blth_name))
                    && mMyBluetoothDevicesAddressNewBuf!!.add(device.address)
                ) {

                    if (rvNAdapter == null || rvNAdapter!!.itemCount == 0) {
                        rvNAdapter = RecyclerViewNewDevicesAdapter(
                            arrayListOf(MyBluetoothDevice(device, false))
                        )
                        rvNewDev.adapter = rvNAdapter
                    } else
                        rvNAdapter!!.addItem(MyBluetoothDevice(device, false))

//                    Log.e("New device", device.name + device.address)

                } else
                    Log.e(
                        "WRONG_SEARCH_BLTH",
                        "Could not get parcelable extra from device: " + BluetoothDevice.EXTRA_DEVICE
                    )

                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    endDiscovery()
                    if (rvNAdapter == null || rvNAdapter!!.itemCount == 0)
                        Snackbar.make(rvSavedDev, R.string.no_new_devices, Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }


    fun saveDevice(address: String) {
        saveLastMacAddress(address)
        saveToListChosenDevices(address)
    }


    fun saveLastMacAddress(address: String) {
        SPUtils.put(
            activity!!.applicationContext,
            SPUtils.SAVED_MAC_ADDRESS_PREF,
            SPUtils.SAVED_MAC_ADDRESS,
            address
        )
    }


    fun saveToListChosenDevices(address: String) {
        val savedAddress = SPUtils.getListString(
            activity!!.applicationContext,
            SPUtils.SAVED_ADRESS_NAME_PREF
        )
        SPUtils.clear(
            activity!!.applicationContext,
            SPUtils.SAVED_ADRESS_NAME_PREF
        )
        savedAddress.add(address)
        SPUtils.putListString(
            activity!!.applicationContext,
            SPUtils.SAVED_ADRESS_NAME_PREF,
            savedAddress
        )
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.menu_settings_activity, menu)
//        return true
//    }


//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//
//        when (item.itemId) {
//            R.id.item_delete_saved_devices -> {
//                SPUtils.clear(applicationContext, SPUtils.SAVED_ADRESS_NAME_PREF)
//                SPUtils.clear(applicationContext, SPUtils.SAVED_MAC_ADDRESS_PREF)
//                rvSavedDevAdapter!!.removeAllItem()
//                fillRecyclerView()
//                mSavedAddress!!.clear()
//                endDiscovery()
//                return true
//            }
//        }
//        return super.onOptionsItemSelected(item)
//    }


    override fun onDestroy() {
        super.onDestroy()

        if (mBtAdapter != null) {
            mBtAdapter!!.cancelDiscovery()
        }
        activity!!.unregisterReceiver(mReceiver)
    }


    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = BlthSettingsDialog()
    }

}