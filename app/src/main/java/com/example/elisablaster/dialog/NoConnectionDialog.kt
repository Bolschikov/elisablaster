package com.example.elisablaster.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.elisablaster.R

class NoConnectionDialog : DialogAppDefault() {

    private var idErrorParam: Int = 0

    interface NoConnectionDialogListener {
        fun onDialogNoConnectionPositiveClick(dialog: NoConnectionDialog, idError: Int)
        fun onDialogNoConnectionNegativeClick(dialog: NoConnectionDialog, idError: Int)
    }

    private var mListener: NoConnectionDialogListener? = null

    companion object {
        private val ARG_PARAM1 = "param_1"
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(idError: Int): NoConnectionDialog {
            val noConnectionDialog = NoConnectionDialog()
            val args = Bundle()
            args.putInt(ARG_PARAM1, idError)
            noConnectionDialog.arguments = args
            return noConnectionDialog
        }
    }

//    fun newInstance(idError: Int): NoConnectionDialog {
//        val noConnectionDialog = NoConnectionDialog()
//        val args = Bundle()
//        args.putInt(ARG_PARAM1, idError)
//        noConnectionDialog.arguments = args
//        return noConnectionDialog
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null)
            idErrorParam = arguments!!.getInt(ARG_PARAM1)
        //        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog_Alert);
        //        setStyle(DialogFragment.STYLE_NORMAL, R.style.MyDialogFragmentStyle);
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog!!.setCanceledOnTouchOutside(false)
        return inflater.inflate(
            R.layout.dialog_no_connection, container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstance: Bundle?) {
        //        getDialog().getWindow().setLayout(260, 300);
        val imageView = view.findViewById<ImageView>(R.id.img_dialog_error)
        val textViewTitle = view.findViewById<TextView>(R.id.txt_dialog_title_error)
        val textViewMsg = view.findViewById<TextView>(R.id.text_dialog_msg_error)
        val btnNo = view.findViewById<Button>(R.id.btn_dialog_no_connection_no)
        val btnYes = view.findViewById<Button>(R.id.btn_dialog_no_connection_yes)
        when (idErrorParam) {
            0 -> {
                Glide.with(activity!!.applicationContext).load(R.drawable.ic_attention).apply(
                    RequestOptions()
                        .fitCenter()
                ).into(imageView)
                textViewTitle.setText(R.string.msg_title_error_no_elisa_dev)
                textViewMsg.setText(R.string.msg_error_accept_bounded_elisa_dev)
                btnNo.setText(R.string.exit)
                btnNo.setOnClickListener { v ->
                    mListener!!.onDialogNoConnectionNegativeClick(
                        this@NoConnectionDialog,
                        0
                    )
                }
                btnYes.setOnClickListener { v ->
                    mListener!!.onDialogNoConnectionPositiveClick(
                        this@NoConnectionDialog,
                        0
                    )
                }
            }
            1 -> {
                Glide.with(activity!!.applicationContext).load(R.drawable.ic_blth_off_white).apply(
                    RequestOptions()
                        .fitCenter()
                ).into(imageView)
                textViewTitle.setText(R.string.msg_not_connected)
                textViewMsg.setText(R.string.dialog_msg_no_connection)
                btnNo.setOnClickListener { v -> dismiss() }
                btnYes.setOnClickListener { v ->
                    mListener!!.onDialogNoConnectionPositiveClick(
                        this@NoConnectionDialog,
                        1
                    )
                }
            }
        }

//        dialog!!.setOnKeyListener { dialog: android.content.DialogInterface, keyCode: Int, event: android.view.KeyEvent ->
//
//            if (keyCode == android.view.KeyEvent.KEYCODE_BACK && idErrorParam == 0)
//                true
//            else false// pretend we've processed it
//        }


    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = context as NoConnectionDialogListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException("$context must implement NoticeDialogListener")
        }

    }
}